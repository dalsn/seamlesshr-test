<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function store()
    {
        factory(User::class)->create();

        return response()->json([
            'status' => "success",
            'message' => 'User created',
            'user' => User::orderBy('id', 'desc')->first()
        ]);
    }
}