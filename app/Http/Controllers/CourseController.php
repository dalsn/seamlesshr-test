<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\CreateCourses;
use Carbon\Carbon;
use App\Course;
use App\Exports\CoursesExport;
use Maatwebsite\Excel\Excel;

class CourseController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $enrolled = $request->user()->courses;

        foreach ($enrolled as $course) {
            $course->pivot;
        }

        $courses = Course::all()->merge($enrolled);

        return response()->json([
            'status' => "success",
            'message' => 'All courses',
            'courses' => $courses
        ]);
    }

    public function store()
    {
        dispatch(new CreateCourses());

        return response()->json([
            'status' => "success",
            'message' => 'Courses creation task queued'
        ]);
    }

    public function enroll(Request $request)
    {
        $coursesIds = $request->only('course_ids');
        $user = $request->user();

        if (is_array($coursesIds)) {
            foreach ($coursesIds as $courseId) {
                $user->courses()->attach($courseId);
            }
        } else {
            $user->courses()->attach($coursesIds);   
        }

        return response()->json([
            'status' => "success",
            'message' => 'Enrollment successful'
        ]);
    }

    public function export(Excel $excel)
    {
        return $excel->download(new CoursesExport, 'courses.xlsx');
    }
}