<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public $date_enrolled;
    
    protected $fillabe = [
        'title', 'text', 'level', 'author',
    ];

    public function users() 
    {
        return $this->belongsToMany(User::class, 'user_course')
        ->withPivot([
                'registration_date'
            ]);
    }
}
