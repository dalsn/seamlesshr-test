<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([

    'middleware' => 'api'

], function () {

    //Authentication Routes
    Route::post('login', 'AuthController@login');

    //User routes
    Route::post('user', 'UserController@store');
    
    Route::group([

        'middleware' => 'auth.api'

    ], function() {

        //Authentication routes
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');


        //Course routes
        Route::post('course', 'CourseController@store');
        Route::post('enroll', 'CourseController@enroll');
        Route::get('course', 'CourseController@index');
        Route::get('export', 'CourseController@export');

    });
});
